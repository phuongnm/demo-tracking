$(document).ready(function(){
    var id = window.location.search.substring(1).split('&')[0].split('=')[1];
    var idImgMap;
    switch(id) {
        case "1":
            idImgMap = 'img/map04.jpg';
            break;
        case "2":
            idImgMap = 'img/map03.jpg';
            break;
        case "3":
            idImgMap = 'img/map05.jpg';
            break;
        case "4":
            idImgMap = 'img/map02.jpg';
            break;
        default:
            idImgMap = 'img/map01.jpg'
    }
    $('#map-img').attr("src", `${window.location.origin}/${idImgMap}`);
});
